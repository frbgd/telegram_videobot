# -*- coding: utf-8 -*-

import os
import pickle
import socket
import logging

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive']


class gdrive_handler:
    def __init__(self):
        try:
            self.logger = logging.getLogger(__name__)
            
            creds = None
            # The file token.pickle stores the user's access and refresh tokens, and is
            # created automatically when the authorization flow completes for the first
            # time.
            if os.path.exists('token.pickle'):
                with open('token.pickle', 'rb') as token:
                    creds = pickle.load(token)
            # If there are no (valid) credentials available, let the user log in.
            if not creds or not creds.valid:
                if creds and creds.expired and creds.refresh_token:
                    creds.refresh(Request())
                else:
                    flow = InstalledAppFlow.from_client_secrets_file(
                        'credentials.json', SCOPES)
                    creds = flow.run_local_server(port=0)
                # Save the credentials for the next run
                with open('token.pickle', 'wb') as token:
                    pickle.dump(creds, token)

            socket.setdefaulttimeout(1200)  # set timeout to 20 minutes
            self.drive_service = build('drive', 'v3', credentials=creds, cache_discovery=False)
        except Exception as e:
            msg = 'Error in gdrive_operations.py: gdrive_handler.__init__({0})\n{1}\n'.format(self, e)
            self.logger.error(msg)
            raise Exception(msg)

    def upload_file(self, path, filename, mime_type):
        try:
            self.logger.info('upload_file({0}, {1}, {2}): started'.format(path, filename, mime_type))
            file_metadata = {'name': filename}
            media = MediaFileUpload(path, mimetype=mime_type)
            file = self.drive_service.files().create(body=file_metadata,
                                                     media_body=media,
                                                     fields='id').execute()
            return file.get('id')
        except Exception as e:
            msg = 'Error in gdrive_operations.py: gdrive_handler.upload_file({0}, {1}, {2}, {3})\n{4}\n'\
                .format(self, path, filename, mime_type, e)
            self.logger.error(msg)
            raise Exception(msg)
