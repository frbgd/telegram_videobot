# -*- coding: utf-8 -*-

import os
import json
import datetime
import logging.handlers
from telethon import TelegramClient, events
from telethon.tl.types import DocumentAttributeFilename

import settings
import message_textes
from db_operations import db_table_handler

app_dir = os.path.dirname(os.path.abspath(__file__))

# create logger
logger = logging.getLogger()
if not logger.handlers:
    logger.setLevel(logging.INFO)
    # log format as: 2013-03-08 11:37:31,411 : : WARNING :: Testing foo
    formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    # handler writes into, limited to 1Mo in append mode
    if not os.path.exists('logs'):
        # create logs directory if does no exist (typically at first start)
        os.makedirs('logs')
    pathLog = app_dir + '/logs/bot.log'
    file_handler = logging.handlers.RotatingFileHandler(pathLog, 'a', 1000000, 5)
    # level debug
    file_handler.setLevel(logging.INFO)
    # using the format defined earlier
    file_handler.setFormatter(formatter)
    # Adding the file handler
    logger.addHandler(file_handler)
logger.info('logger initialized\n\n\n')

bot = TelegramClient('bot', settings.telegram_api_id, settings.telegram_api_hash)
logger.info('bot initialized')


def my_converter(o):
    logger.info('my_converter({}): started'.format(o))
    if isinstance(o, datetime.datetime):
        return o.__str__()


@bot.on(events.NewMessage)
async def message_handler(event):
    try:
        if 'Error in' in event.message.text:
            return
        logger.info(
            'Received message from {0} {1} {2} {3}'.format(event.chat_id, event.chat.username, event.chat.first_name,
                                                           event.chat.last_name))

        db_chat_info = db_handler.db_select({"chat_id": event.chat_id,
                                             "username": event.chat.username,
                                             "name": '{0} {1}'.format(event.chat.first_name, event.chat.last_name)})
        if len(db_chat_info) == 0:
            logger.info('Inserting user info: {0} {1} {2} {3} into db'.format(event.chat_id, event.chat.username,
                                                                              event.chat.first_name,
                                                                              event.chat.last_name))
            db_handler.db_insert({"chat_id": event.chat_id,
                                  "username": event.chat.username,
                                  "name": '{0} {1}'.format(event.chat.first_name, event.chat.last_name),
                                  "uploaded": -2,
                                  "downloaded": 0})

            msg = message_textes.start
            await event.reply(msg)
            logger.info('Replied to {0} {1} {2} {3} next message:\n{4}'.format(event.chat_id, event.chat.username,
                                                                               event.chat.first_name,
                                                                               event.chat.last_name, msg))

        else:
            logger.info('Query with chat_id: {0}, username: {1}, name: {2} {3} found into db'
                        .format(event.chat_id, event.chat.username, event.chat.first_name, event.chat.last_name))
            if db_chat_info[0][5] == -2:
                logger.info('Receiving inputname from user: {0} {1} {2} {3}'.format(event.chat_id, event.chat.username,
                                                                                    event.chat.first_name,
                                                                                    event.chat.last_name))
                if event.message.media is None:
                    logger.info('Inserting inputname: {0} from user: {1} {2} {3} {4} into db'
                                .format(event.message.text, event.chat_id, event.chat.username, event.chat.first_name,
                                        event.chat.last_name))
                    db_handler.db_update({"chat_id": event.chat_id,
                                          "username": event.chat.username,
                                          "name": '{0} {1}'.format(event.chat.first_name, event.chat.last_name)},
                                         {"inputname": event.message.text, "uploaded": -1})

                    msg = message_textes.correct_name.format(event.message.text)
                    await event.reply(msg)
                    logger.info('Replied to {0} {1} {2} {3} next message:\n{4}'.format(event.chat_id,
                                                                                       event.chat.username,
                                                                                       event.chat.first_name,
                                                                                       event.chat.last_name, msg))

                else:
                    logger.info('Incorrect inputname message from user: {0} {1} {2} {3}'
                                .format(event.chat_id, event.chat.username, event.chat.first_name,
                                        event.chat.last_name))
                    msg = message_textes.incorrect_name
                    await event.reply(msg)
                    logger.info('Replied to {0} {1} {2} {3} next message:\n{4}'.format(event.chat_id,
                                                                                       event.chat.username,
                                                                                       event.chat.first_name,
                                                                                       event.chat.last_name, msg))

            elif db_chat_info[0][5] == -1:
                logger.info('Receiving video from user: {0} {1} {2} {3}'.format(event.chat_id, event.chat.username,
                                                                                event.chat.first_name,
                                                                                event.chat.last_name))
                if event.message.video is not None and event.message.video_note is None:
                    file_name = [item.file_name for item in event.message.video.attributes
                                 if isinstance(item, DocumentAttributeFilename)]
                    if len(file_name) != 0:
                        file_name = file_name.pop()
                    else:
                        file_name = '.{}'.format(event.message.video.mime_type.split('/')[1])
                    file_reference = str(event.message.video.file_reference).replace("b\'", "'").replace("\'", "")

                    logger.info('Inserting video info from user: {0} {1} {2} {3} into db'
                                .format(event.chat_id, event.chat.username, event.chat.first_name,
                                        event.chat.last_name))
                    db_handler.db_update({"chat_id": event.chat_id,
                                          "username": event.chat.username,
                                          "name": '{0} {1}'.format(event.chat.first_name, event.chat.last_name)},
                                         {"uploaded": 0,
                                          "media_json": json.dumps(event.message.media.to_dict(),
                                                                   default=my_converter),
                                          "mime_type": event.message.video.mime_type,
                                          "file_name": file_name,
                                          "file_reference": file_reference})

                    msg = message_textes.correct_video
                    await event.reply(msg)
                    logger.info('Replied to {0} {1} {2} {3} next message:\n{4}'.format(event.chat_id,
                                                                                       event.chat.username,
                                                                                       event.chat.first_name,
                                                                                       event.chat.last_name, msg))

                else:
                    logger.info('Incorrect video message from user: {0} {1} {2} {3}'
                                .format(event.chat_id, event.chat.username, event.chat.first_name,
                                        event.chat.last_name))
                    msg = message_textes.incorrect_video
                    await event.reply(msg)
                    logger.info('Replied to {0} {1} {2} {3} next message:\n{4}'.format(event.chat_id,
                                                                                       event.chat.username,
                                                                                       event.chat.first_name,
                                                                                       event.chat.last_name, msg))

            else:
                logger.info('Video info from chat_id: {} already in db'.format(event.chat_id))
                msg = message_textes.stop
                await event.reply(msg)
                logger.info('Replied to {0} {1} {2} {3} next message:\n{4}'.format(event.chat_id,
                                                                                   event.chat.username,
                                                                                   event.chat.first_name,
                                                                                   event.chat.last_name, msg))

    except Exception as e:
        msg = message_textes.error
        await event.reply(msg)
        logger.error('Replied to {0} {1} {2} {3} next message:\n{4}'.format(event.chat_id,
                                                                            event.chat.username,
                                                                            event.chat.first_name,
                                                                            event.chat.last_name, msg))

        msg = 'Error in bot.py: message_handler({0})\n{1}\n'.format(event, e)
        logger.error(msg, exc_info=True)
        bot.loop.run_until_complete(bot.send_message(settings.telegram_admin_chat_id, msg))
        logger.error('Sent to admin (chat_id:{0}) next message:\n{1}'.format(settings.telegram_admin_chat_id, msg))


db_handler = db_table_handler(settings.database_path, settings.database_table, settings.database_schema)
db_handler.check_table_existing()
logger.info('db_handler initialized')

bot.start(bot_token=settings.telegram_bot_token)
logger.info('bot started')
bot.run_until_disconnected()
