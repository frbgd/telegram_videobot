#!/bin/bash

TOKEN=$TELEGRAM_BOT_TOKEN
CHAT_ID=$TELEGRAM_ADMIN_CHAT_ID
URL="https://api.telegram.org/bot$TOKEN/sendMessage"

RESULT=$(systemctl status video_bot | grep "   Active: " | awk '{print $2}')
if [[ "$RESULT" != "active" ]]; then
MESSAGE="Error in GLOBAL!!! Service video_bot isn't active!"
curl -s -X POST $URL -d chat_id=$CHAT_ID -d text="$MESSAGE"
fi

RESULT=$(systemctl status video_uploader | grep "   Active: " | awk '{print $2}')
if [[ "$RESULT" != "active" ]]; then
MESSAGE="Error in GLOBAL!!! Service video_uploader isn't active!"
curl -s -X POST $URL -d chat_id=$CHAT_ID -d text="$MESSAGE"
fi