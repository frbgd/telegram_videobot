# -*- coding: utf-8 -*-

import os

telegram_bot_token = os.getenv('TELEGRAM_BOT_TOKEN')
telegram_admin_chat_id = os.getenv('TELEGRAM_ADMIN_CHAT_ID')
telegram_api_id = os.getenv('TELEGRAM_API_ID')
telegram_api_hash = os.getenv('TELEGRAM_API_HASH')

database_path = 'videos.db'
database_table = 'videos'
database_schema = "chat_id INTEGER, username TEXT, name TEXT, inputname TEXT, media_json TEXT, uploaded " \
                  "INTEGER, mime_type TEXT, file_name TEXT, file_reference TEXT, downloaded INTEGER"
