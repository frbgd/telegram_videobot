# -*- coding: utf-8 -*-

import os
import json
import logging.handlers
from telethon import TelegramClient
from telethon.tl.types import MessageMediaDocument, Document, DocumentAttributeFilename, DocumentAttributeVideo

import settings
from db_operations import db_table_handler
from gdrive_operations import gdrive_handler

app_dir = os.path.dirname(os.path.abspath(__file__))

# create logger
logger = logging.getLogger()
if not logger.handlers:
    logger.setLevel(logging.INFO)
    # log format as: 2013-03-08 11:37:31,411 : : WARNING :: Testing foo
    formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    # handler writes into, limited to 1Mo in append mode
    if not os.path.exists('logs'):
        # create logs directory if does no exist (typically at first start)
        os.makedirs('logs')
    pathLog = app_dir + '/logs/uploader.log'
    file_handler = logging.handlers.RotatingFileHandler(pathLog, 'a', 1000000, 5)
    # level debug
    file_handler.setLevel(logging.INFO)
    # using the format defined earlier
    file_handler.setFormatter(formatter)
    # Adding the file handler
    logger.addHandler(file_handler)
logger.info('logger initialized\n\n\n')


def loop_function():
    try:
        global item
        logger.info('loop_function(): started')
        while True:
            for item in db_handler.db_select({"uploaded": 0}):
                try:
                    logger.info('loop_function(): item {} selected'.format(item))
                    if item[1] == 'None':
                        username = ''
                    else:
                        username = '-{}'.format(item[1])
                    if not str(item[7]).startswith('.'):
                        file_name = '-{}'.format(item[7])
                    else:
                        file_name = item[7]
                    file_name = '{0}{1}{2}'.format(item[3], username, file_name)

                    db_handler.db_update({"chat_id": item[0],
                                          "username": item[1],
                                          "name": item[2]}, {"uploaded": 1})
                    logger.info('loop_function(): File "{}" db updated (start)'.format(file_name))

                    if item[9] == 0:
                        download_file(item, file_name)
                        logger.info('loop_function(): File "{}" downloaded from Telegram servers'.format(file_name))

                        db_handler.db_update({"chat_id": item[0],
                                              "username": item[1],
                                              "name": item[2]}, {"downloaded": 1})
                        logger.info('loop_function(): File "{}" db updated (download)'.format(file_name))

                    gd_handler.upload_file('video-files/{}'.format(file_name), file_name, item[4])
                    logger.info('loop_function(): File "{}" uploaded to Google Drive'.format(file_name))

                    os.remove('video-files/{}'.format(file_name))
                    logger.info('loop_function(): File "{}" deleted from server storage '.format(file_name))

                    db_handler.db_update({"chat_id": item[0],
                                          "username": item[1],
                                          "name": item[2]}, {"uploaded": 2, "downloaded": 0})

                    logger.info('loop_function(): File "{}" db updated (stop)'.format(file_name))
                except Exception as e:
                    msg = 'Error in uploader.py: loop_function().for({0})\n{1}\n'.format(item, e)
                    logger.error(msg, exc_info=True)
                    with TelegramClient('uploader', settings.telegram_api_id,
                                        settings.telegram_api_hash).start(
                                        bot_token=settings.telegram_bot_token) as client:
                        client.loop.run_until_complete(client.send_message(settings.telegram_admin_chat_id, msg))
                    logger.error(
                        'Sent to admin (chat_id:{0}) next message:\n{1}'.format(settings.telegram_admin_chat_id, msg))
                    if '[Errno 32] Broken pipe' in str(e):
                        db_handler.db_update({"chat_id": item[0],
                                              "username": item[1],
                                              "name": item[2]}, {"uploaded": 0})
    except Exception as e:
        msg = 'Error in uploader.py: loop_function()\n{}\n'.format(e)
        logger.error(msg, exc_info=True)
        with TelegramClient('uploader', settings.telegram_api_id,
                            settings.telegram_api_hash).start(bot_token=settings.telegram_bot_token) as client:
            client.loop.run_until_complete(client.send_message(settings.telegram_admin_chat_id, msg))
        logger.error(
            'Sent to admin (chat_id:{0}) next message:\n{1}'.format(settings.telegram_admin_chat_id, msg))


def download_file(item, file_name):
    try:
        logger.info('download_file({0}, {1}): started'.format(item, file_name))

        media = json.loads(item[4])
        new_attributes = []
        for attribute in media['document']['attributes']:
            if attribute['_'] == 'DocumentAttributeFilename':
                new_attributes.append(DocumentAttributeFilename(attribute['file_name']))
            elif attribute['_'] == 'DocumentAttributeVideo':
                new_attributes.append(DocumentAttributeVideo(duration=attribute['duration'],
                                                             w=attribute['w'],
                                                             h=attribute['h'],
                                                             round_message=attribute['round_message'],
                                                             supports_streaming=attribute['supports_streaming']))
        media['document']['attributes'] = new_attributes
        media['document'] = Document(id=media['document']['id'],
                                     access_hash=media['document']['access_hash'],
                                     file_reference=item[8],
                                     date=media['document']['date'],
                                     mime_type=media['document']['mime_type'],
                                     size=media['document']['size'],
                                     dc_id=media['document']['dc_id'],
                                     attributes=media['document']['attributes'],
                                     thumbs=media['document']['thumbs'])
        media = MessageMediaDocument(document=media['document'])
        with TelegramClient('uploader', settings.telegram_api_id,
                            settings.telegram_api_hash).start(bot_token=settings.telegram_bot_token) as client:
            client.loop.run_until_complete(client.download_media(message=media, file='video-files/{}'
                                                                 .format(file_name)))
    except Exception as e:
        msg = 'Error in uploader.py: download_file({0}, {1})\n{2}\n'.format(item, file_name, e)
        logger.error(msg)
        raise Exception(msg)


db_handler = db_table_handler(settings.database_path, settings.database_table, settings.database_schema)
logger.info('db_handler initialized')
db_handler.check_table_existing()
logger.info('table checked')

gd_handler = gdrive_handler()
logger.info('gd_handler initialized')

loop_function()
