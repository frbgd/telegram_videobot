# -*- coding: utf-8 -*-

import sqlite3
import logging


class db_table_handler:
    def __init__(self, db_path, db_table, db_schema):
        try:
            self.db_path = db_path
            self.db_table = db_table
            self.db_schema = db_schema
            self.logger = logging.getLogger(__name__)
        except Exception as e:
            msg = 'Error in db_operations.py: db_table_handler.__init__({0}, {1}, {2}, {3})\n{4}\n'\
                .format(self, db_path, db_table, db_schema, e)
            self.logger.error(msg)
            raise Exception(msg)

    def check_table_existing(self):
        try:
            self.logger.info('check_table_existing(): started')
            sql = "SELECT * FROM main.sqlite_master WHERE type='table' and name='{}'".format(self.db_table)

            db_conn = sqlite3.connect(self.db_path)
            self.logger.info('check_table_existing(): db connected')
            db_cursor = db_conn.cursor()
            self.logger.info('check_table_existing(): cursor initialized')
            db_cursor.execute(sql)
            cursor_result = db_cursor.fetchall()
            self.logger.info('check_table_existing(): SQL query "{}" executed'.format(sql))

            if len(cursor_result) == 0:
                self.logger.info('check_table_existing(): No tables found. Creating new one')
                sql = "CREATE TABLE {0}({1})".format(self.db_table, self.db_schema)

                db_cursor.execute(sql)
                db_conn.commit()
                self.logger.info('check_table_existing(): SQL query "{}" executed'.format(sql))
        except Exception as e:
            msg = 'Error in db_operations.py: db_table_handler.check_table_existing({0})\n{1}\n'.format(self, e)
            self.logger.error(msg)
            raise Exception(msg)

    def db_select(self, where_filter):
        try:
            if len(where_filter) == 0:
                # self.logger.info('db_select({}): no where filters, returning'.format(where_filter))
                return
            # self.logger.info('db_select({}): started'.format(where_filter))
            filter_clause = ["{0} = '{1}'".format(key, where_filter[key]) for key in where_filter]
            sql = "SELECT * FROM {0} WHERE {1}".format(self.db_table, ' AND '.join(filter_clause))

            db_conn = sqlite3.connect(self.db_path)
            # self.logger.info('db_select({}): db connected'.format(where_filter))
            db_cursor = db_conn.cursor()
            # self.logger.info('db_select({}): cursor initialized'.format(where_filter))
            db_cursor.execute(sql)
            result = db_cursor.fetchall()
            # self.logger.info('db_select({0}): SQL query: "{1}" executed'.format(where_filter, sql))

            return result
        except Exception as e:
            msg = 'Error in db_operations.py: db_table_handler.db_select({0}, {1})\n{2}\n'\
                .format(self, where_filter, e)
            self.logger.error(msg)
            raise Exception(msg)

    def db_insert(self, fields):
        try:
            self.logger.info('db_insert({}): started'.format(fields))
            if len(fields) == 0:
                self.logger.info('db_insert({}): no fields, returning'.format(fields))
                return
            sql = "INSERT INTO {0} ({1}) VALUES ('{2}')".format(self.db_table, ",".join(fields.keys()),
                                                                "','".join([str(value) for value in fields.values()]))

            db_conn = sqlite3.connect(self.db_path)
            self.logger.info('db_insert({}): db connected'.format(fields))
            db_cursor = db_conn.cursor()
            self.logger.info('db_insert({}): cursor initialized'.format(fields))
            db_cursor.execute(sql)
            db_conn.commit()
            self.logger.info('db_insert({0}): SQL query: "{1}" executed'.format(fields, sql))
        except Exception as e:
            msg = 'Error in db_operations.py: db_table_handler.db_insert({0}, {1})\n{2}\n'.format(self, fields, e)
            self.logger.error(msg)
            raise Exception(msg)

    def db_update(self, where_filter, fields):
        try:
            self.logger.info('db_update({0}, {1}): started'.format(where_filter, fields))
            if len(fields) == 0 or len(where_filter) == 0:
                self.logger.info('db_update({0}, {1}): no fields or where filters, returning'.format(where_filter,
                                                                                                     fields))
                return
            filter_clause = ["{0} = '{1}'".format(key, where_filter[key]) for key in where_filter]
            update_clause = ["{0} = '{1}'".format(key, fields[key]) for key in fields]
            sql = "UPDATE {0} SET {1} WHERE {2}".format(self.db_table, ','.join(update_clause),
                                                        ' AND '.join(filter_clause))

            db_conn = sqlite3.connect(self.db_path)
            self.logger.info('db_update({0}, {1}): db connected'.format(where_filter, fields))
            db_cursor = db_conn.cursor()
            self.logger.info('db_update({0}, {1}): cursor initialized'.format(where_filter, fields))
            db_cursor.execute(sql)
            db_conn.commit()
            self.logger.info('db_update({0}, {1}): SQL query: "{2}" executed'.format(where_filter, fields, sql))
        except Exception as e:
            msg = 'Error in db_operations.py: db_table_handler.db_update({0}, {1}, {2})\n{3}\n'\
                .format(self, where_filter, fields, e)
            self.logger.error(msg)
            raise Exception(msg)

    def db_delete(self, where_filter):
        try:
            if len(where_filter) == 0:
                self.logger.info('db_delete({}): no where filters, returning'.format(where_filter))
                return
            filter_clause = ["{0} = '{1}'".format(key, where_filter[key]) for key in where_filter]
            sql = 'DELETE FROM {0} WHERE {1}'.format(self.db_table, ' AND '.join(filter_clause))

            db_conn = sqlite3.connect(self.db_path)
            self.logger.info('db_delete({0}): db connected'.format(where_filter))
            db_cursor = db_conn.cursor()
            self.logger.info('db_delete({0}): cursor initialized'.format(where_filter))
            db_cursor.execute(sql)
            db_conn.commit()
            self.logger.info('db_delete({0}): SQL query: "{1}" executed'.format(where_filter, sql))
        except Exception as e:
            msg = 'Error in db_operations.py: db_table_handler.db_delete({0}, {1})\n{2}\n'\
                .format(self, where_filter, e)
            self.logger.error(msg)
            raise Exception(msg)
